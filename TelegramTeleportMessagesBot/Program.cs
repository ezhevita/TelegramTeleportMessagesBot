﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using File = System.IO.File;

namespace TelegramTeleportMessagesBot {
	internal static class Program {
		private static TelegramBotClient Bot;
		private static Dictionary<long, long> TeleportChats;
		private static Dictionary<string, long> WaitingForLinking;
		private static int BotID;
		private static readonly Random Random = new Random();

		private static string RandomString(int length) {
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string(Enumerable.Repeat(chars, length)
			.Select(s => s[Random.Next(s.Length)]).ToArray());
		}

		public static async Task Main() {
			if (File.Exists("log.txt")) {
				File.Delete("log.txt");
			}

			Log("Started TTMB");
			string token;
			if (File.Exists("token.txt")) {
				token = File.ReadAllText("token.txt");
			} else {
				Console.WriteLine("Please, enter your bot token:");
				token = Console.ReadLine();
			}

			TeleportChats = File.Exists("data.json")
				? new Dictionary<long, long>(
					JsonConvert.DeserializeObject<Dictionary<long, long>>(File.ReadAllText("data.json")))
				: new Dictionary<long, long>();
			WaitingForLinking = new Dictionary<string, long>();
			Bot = new TelegramBotClient(token);
			User botUser = await Bot.GetMeAsync().ConfigureAwait(false);
			BotID = botUser.Id;
			string username = botUser.Username;
			Log($"Hello, I am @{username}!");
			Bot.OnMessage += BotOnMessage;
			Bot.StartReceiving();
			Thread.CurrentThread.Join();
		}

		private static async void BotOnMessage(object sender, MessageEventArgs e) {
			Message message = e?.Message;
			try {
				if (message == null) {
					return;
				}

				if (message.Type != MessageType.Text) {
					return;
				}

				string messageSender = message.From.FirstName +
				                       (string.IsNullOrEmpty(message.From.LastName)
					                       ? string.Empty
					                       : " " + message.From.LastName) + (string.IsNullOrEmpty(message.From.Username)
					                       ? string.Empty
					                       : $" (@{message.From.Username})");
				switch (message.Chat.Type) {
					case ChatType.Supergroup:
						List<ChatMember> admins = (await Bot.GetChatAdministratorsAsync(message.Chat.Id).ConfigureAwait(false)).ToList();
						string[] commandArgs = message.Text.Split(' ', '@');
						string code = "";
						switch (commandArgs[0].ToUpperInvariant()) {
							case "/TELEPORTFROM" when admins.Select(x => x.User.Id).Contains(message.From.Id) || (message.From.Id == 204723509):
								Log($"Got new teleport from message from {messageSender}");
								bool isPresented = true;
								while (isPresented) {
									code = RandomString(8);
									isPresented = WaitingForLinking.ContainsKey(code);
								}

								WaitingForLinking.Add(code, message.Chat.Id);
								await Bot.SendTextMessageAsync(message.Chat.Id,
									$"Selected to teleport from this chat, now use command `/teleportto {code}` in the target chat.",
									ParseMode.Markdown,
									replyToMessageId: message.MessageId).ConfigureAwait(false);


								break;
							case "/TELEPORTTO" when admins.Select(x => x.User.Id).Contains(message.From.Id) || (message.From.Id == 204723509):

								Log($"Got new teleport to message from {messageSender}");
								if (message.Text.Split(' ').Length < 2) {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Invalid code.", replyToMessageId: message.MessageId).ConfigureAwait(false);
								}

								code = message.Text.Split(' ')[1];
								if (!WaitingForLinking.ContainsKey(code)) {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Invalid code.", replyToMessageId: message.MessageId)
									.ConfigureAwait(false);
								} else {
									TeleportChats.Add(WaitingForLinking[code], message.Chat.Id);
									WaitingForLinking.Remove(code);
									File.WriteAllText("data.json", JsonConvert.SerializeObject(TeleportChats));
									await Bot.SendTextMessageAsync(message.Chat.Id, "Done linking!", replyToMessageId: message.MessageId)
									.ConfigureAwait(false);
								}

								break;
							case "/TELEPORT" when admins.Select(x => x.User.Id).Contains(message.From.Id) || (message.From.Id == 204723509) ||
							                      ((message.From.Id == 335302442) && (message.Chat.Id == -1001138768631)):

								Log($"Got new teleport message from {messageSender}");
								if (!TeleportChats.ContainsKey(message.Chat.Id)) {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Please link chats first before teleporting.",
										replyToMessageId: message.MessageId).ConfigureAwait(false);
									return;
								}

								if (message.ReplyToMessage == null) {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Please reply to message to teleport starting from it.",
										replyToMessageId: message.MessageId).ConfigureAwait(false);
									return;
								}

								if (admins.Select(x => x.User.Id).Contains(BotID)) {
									ChatMember botAdmin = admins.FirstOrDefault(x => x.User.Id == BotID);
									if (botAdmin?.CanDeleteMessages != true) {
										await Bot.SendTextMessageAsync(message.Chat.Id, "Please grant bot a `Delete messages` permission.",
											ParseMode.Markdown,
											replyToMessageId: message.MessageId).ConfigureAwait(false);
										return;
									}
								} else {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Please make bot an admin and grant `Delete messages` permission.",
										ParseMode.Markdown,
										replyToMessageId: message.MessageId).ConfigureAwait(false);
									return;
								}

								int testID;
								try {
									testID = (await Bot.SendTextMessageAsync(TeleportChats[message.Chat.Id], "Teleporting...").ConfigureAwait(false)).MessageId;
								} catch {
									await Bot.SendTextMessageAsync(message.Chat.Id, "Please add bot to the target group.", replyToMessageId: message.MessageId).ConfigureAwait(false);
									return;
								}

								for (int i = message.ReplyToMessage.MessageId; i < message.MessageId; i++) {
									try {
										await Bot.ForwardMessageAsync(TeleportChats[message.Chat.Id], message.Chat.Id, i).ConfigureAwait(false);
										await Bot.DeleteMessageAsync(message.Chat.Id, i).ConfigureAwait(false);
									} catch {
										// ignored
									}
								}

								await Bot.DeleteMessageAsync(TeleportChats[message.Chat.Id], testID).ConfigureAwait(false);
								await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId).ConfigureAwait(false);
								break;
						}

						break;
					case ChatType.Group:
						if (!(await Bot.GetChatAdministratorsAsync(message.Chat.Id).ConfigureAwait(false)).Select(x => x.User.Id).ToList().Contains(message.From.Id)) {
							return;
						}

						await Bot.SendTextMessageAsync(message.Chat.Id, "Sorry, but bot only works with supergroups.",
							replyToMessageId: message.MessageId).ConfigureAwait(false);
						Log($"Got message not from supergroup {message.Chat.FirstName} by {messageSender}");

						return;
				}
			} catch (Exception ex) {
				Log($"Got an exception: {ex.Message}");
				Log($"Stacktrace: {ex.StackTrace}");
			}
		}

		private static void Log(string log) {
			log = $"[{DateTime.Now:G}] {log}";
			Console.WriteLine(log);
			File.AppendAllText("log.txt", log + Environment.NewLine, Encoding.Unicode);
		}
	}
}
